"""This script accepts any message from the keyboard, calculates a checksum for it and prints it out.
The checksum is basically just a 2-symbols hex representation of the remainder
of dividing the sum of ASCII codes of all message's characters by 256.
"""

from typing import AnyStr

from utils import text_styles


def calculate_checksum(text: AnyStr) -> AnyStr:
    input_is_bytes = True
    if isinstance(text, str):
        input_is_bytes = False
        text = text.encode('ascii', errors='ignore')
    elif not isinstance(text, bytes):
        raise ValueError(f"Invalid type of text: '{type(text).__name__}'")
    result = hex(sum(text) % 256)[2:].rjust(2, '0')
    if input_is_bytes:
        result = result.encode('ascii', errors='ignore')
    return result


def main():
    print(text_styles.yellow("Hint: Enter your messages one per line to get them back with their checksums"))
    while True:
        text = input()
        try:
            print(text_styles.bold(text + text_styles.green(calculate_checksum(text))))
        except UnicodeEncodeError:
            print(text_styles.red("Invalid ASCII string! Try again"))


if __name__ == "__main__":
    try:
        main()
    except (EOFError, KeyboardInterrupt):
        print(text_styles.yellow("\nExit"))
        exit()
