"""This script is needed to renew the device firmware by the Bluetooth through UART.

Usage:
    python firmware_bt_renew.py /path/to/firmware.hex <serial_port> [--start_at_once]

At the start script waits until "FLASH?" was received and then starts sending given file to the bootloader.
When file was sent, the script will ask user to input the version of Cobra and then send it to the bootloader too.
At the end the sending statistics will be shown to user.

The "--start_at_once" is optional and should appear only at the end of script call. It forces script to start flashing
without asking the microcontroller for it. It is useful if you see the "START:" message on your UART terminal.

Note: this is the only script in this repo (by the 11.06.2021) which really depends on requirements.txt.
If you don't want to use it, you can easily skip installing all dependencies
and use pure python for all other scripts in the repo.
"""

import os
import sys
from time import time

from serial import Serial, SerialException

from utils import text_styles


MAX_TRIES = 32
TIMEOUT = 10
TIMEOUT_LONG = TIMEOUT * 3
MESSAGE = f"{text_styles.blinking('Sending')}... "

report_values = {
    "ok_counter": 0,
    "error_counter": 0,
    "max_error_counter": 0,

    "time_sum": 0,
    "max_time": 0,
    "min_time": TIMEOUT,

    "current_message": ""
}


def clear_connecting():
    print(end=chr(8)*16, flush=True)
    print(end=' '*16, flush=True)
    print(end=chr(8)*16, flush=True)


def clear_sending():
    print(end=chr(8)*(len(report_values["current_message"]) + len(MESSAGE)), flush=True)
    print(end=' '*(len(report_values["current_message"]) + len(MESSAGE)), flush=True)
    print(end=chr(8)*(len(report_values["current_message"]) + len(MESSAGE)), flush=True)


def read_firmware(file_name: str):
    try:
        with open(file_name) as file:
            while file.readable():
                new_line = file.readline()
                if not new_line:
                    return
                yield new_line
    except FileNotFoundError:
        print("Could not find file '{}', exiting".format(file_name))
        exit()


def print_report(file_lines: int):
    print()
    print(
        "Successfully sent lines: "
        f"\033[{92 if report_values['ok_counter'] == file_lines else 91}m"
        f"{report_values['ok_counter']}/{file_lines}\033[0m"
    )
    print(
        "Errors received: \033["
        f"{93 if report_values['error_counter'] else 92}m{report_values['error_counter']}\033[0m"
    )
    print(
        "Maximum errors per line: \033["
        f"{93 if report_values['max_error_counter'] else 92}m{report_values['max_error_counter']}\033[0m"
    )
    print()
    if report_values['ok_counter']:
        print("Average seconds per line: \033[4m\033[36m{}\033[0m".format(
            round(report_values['time_sum'] / report_values['ok_counter'], 3))
        )
    print(f"Max seconds per line: {text_styles.underline(text_styles.cyan(str(round(report_values['max_time'], 3))))}")
    print(f"Min seconds per line: {text_styles.underline(text_styles.cyan(str(round(report_values['min_time'], 3))))}")


def main():
    file_lines = 0
    if len(sys.argv) < 3:
        print(f"Usage:\n    python {sys.argv[0]} /path/to/firmware.hex <serial_port>")
        print(f"Example:\n    python {sys.argv[0]} ./Cobra_103.hex /dev/ttyUSB0")
        exit(1)

    if not os.path.isfile(sys.argv[1]):
        print(text_styles.red(f"Can not open '{sys.argv[1]}'"))
        exit(1)
    if not os.path.exists(sys.argv[2]):
        print(text_styles.red(f"Can not open '{sys.argv[2]}'"))
        exit(1)

    with open(sys.argv[1]) as lines_counter:
        file_lines += len(lines_counter.readlines())
        lines_counter.seek(0)

    sender = None
    try:
        sender = Serial(sys.argv[2], timeout=TIMEOUT_LONG, baudrate=9600)
    except SerialException:
        print(text_styles.red(f'Can not connect to serial port {sys.argv[2]}'))
        exit(1)

    if len(sys.argv) != 4 or sys.argv[-1] != "--start_at_once":
        try:
            print("Connecting...", end='', flush=True)

            if sender.read_until(b'\r\n').decode().rstrip('\r\n').upper().find("FLASH?") != -1:
                sender.write(b"FLASH!\r\n")
            else:
                clear_connecting()
                print(text_styles.yellow("Controller did not ask for flash, exiting"))
                exit()

        except (KeyboardInterrupt, EOFError):
            clear_connecting()
            print(text_styles.yellow('Cancelled'))
            exit()

        except SerialException:
            clear_connecting()
            print(text_styles.red('Connection lost'))
            exit(1)

        except UnicodeDecodeError:
            clear_connecting()
            print("Controller did not ask for flash, exiting")
            exit()

        clear_connecting()
        sender.timeout = TIMEOUT

        print(text_styles.yellow("Waiting..."), end='', flush=True)

        try:
            if sender.read_until(b'\r\n').decode().rstrip('\r\n').upper().find("START:") == -1:
                clear_sending()
                print(text_styles.yellow("Controller did not ask for flash, exiting"))
                exit()

        except (KeyboardInterrupt, EOFError):
            clear_connecting()
            print(text_styles.yellow('Cancelled'))
            exit()

        except SerialException:
            clear_connecting()
            print(text_styles.red('Connection lost'))
            exit(1)

        clear_sending()
    
    print(MESSAGE, end='', flush=True)
    try:
        for line in read_firmware(sys.argv[1]):
            i = 0
            start_time = time()

            def calculate_report_values():
                report_values['max_error_counter'] = max(report_values['max_error_counter'], i)
                time_for_line = time() - start_time
                report_values['time_sum'] += time_for_line
                report_values['max_time'] = max(time_for_line, report_values['max_time'])
                report_values['min_time'] = min(time_for_line, report_values['min_time'])

            for i in range(MAX_TRIES):
                sender.write((line.rstrip('\r\n') + '\r\n').encode('ascii'))
                response = sender.read_until(b'\r\n').decode().rstrip('\r\n')
                if response.upper() == "OK":
                    report_values['ok_counter'] += 1
                    break
                elif not response:
                    clear_sending()
                    print(text_styles.red("Gave up waiting for response, exiting"))
                    calculate_report_values()
                    print_report(file_lines)
                    exit()
                elif response.upper().find("FLASH") != -1 and response.upper().find("COMPLETE") != -1:
                    break
                elif response.upper() != "ERROR":
                    clear_sending()
                    print(f"Incorrect response received: {text_styles.red(response)}")
                    calculate_report_values()
                    print_report(file_lines)
                    exit()
                report_values['error_counter'] += 1

            if i >= MAX_TRIES - 1:
                clear_sending()
                print(f"Gave up sending line after {text_styles.red(str(i))} attempts, exiting")
                calculate_report_values()
                print_report(file_lines)
                exit(1)

            calculate_report_values()

            print(end=chr(8)*len(report_values["current_message"]), flush=True)
            print(end=' '*len(report_values["current_message"]), flush=True)
            print(end=chr(8)*len(report_values["current_message"]), flush=True)
            report_values["current_message"] = f"({report_values['ok_counter']}/{file_lines} lines)"
            print(end=report_values["current_message"], flush=True)

        old_timeout = sender.timeout
        sender.timeout = 2
        try:
            msg = ''
            while "COBRA VERSION" not in msg:
                msg = sender.read_until(b'\r\n').decode().rstrip('\r\n').upper()
            clear_sending()
            sender.write(
                (input(
                    chr(8) * len(report_values["current_message"]) + "Input Cobra version (no more than 128 symbols): "
                ).rstrip('\r\n') + '\r\n').encode('ascii')
            )
        except SerialException:
            pass
        sender.timeout = old_timeout

    except (KeyboardInterrupt, EOFError):
        clear_sending()
        print(text_styles.yellow('Cancelled'))
        print_report(file_lines)
        exit()

    except SerialException:
        clear_sending()
        print(text_styles.red('Connection lost'))
        print_report(file_lines)
        exit(1)

    sender.close()
    clear_sending()
    print(text_styles.green("DONE"))
    print_report(file_lines)


if __name__ == "__main__":
    main()
