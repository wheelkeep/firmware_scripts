import os
import tty
import termios
import argparse
from sys import stdin
from time import sleep
from pathlib import Path
from threading import Thread
from datetime import datetime
from typing import BinaryIO, Optional, List, Iterable
from multiprocessing.connection import Connection
from multiprocessing import Process, Pipe, Lock, Value

from serial import Serial, SerialException

from utils import text_styles
from cs_calc import calculate_checksum
from utils.prefix_search import PrefixTree


HISTORY_FILE_NAME = '.serial_terminal_history'
MAX_HISTORY_LINES = 4096
INVITATION = text_styles.yellow("> ")

BACKSPACE = 0x7f
BACKSPACE_CTRL = 0x08
CONTROL_SYMBOL = 0x1b
CTRL_L = 0x0c
MAX_CONTROL_SEQUENCE_SIZE = 6

SHIFT_ENTER = b'\x1bOM'
PAGE_UP = b'\x1b[5~'
PAGE_DOWN = b'\x1b[6~'
CTRL_PAGE_UP = b'\x1b[5;5~'
CTRL_PAGE_DOWN = b'\x1b[6;5~'
ALT_PAGE_UP = b'\x1b[5;3~'
ALT_PAGE_DOWN = b'\x1b[6;3~'
HOME_BTN = b'\x1b[H'
END_BTN = b'\x1b[F'

PAGEUP_BUTTONS = (PAGE_UP, CTRL_PAGE_UP, ALT_PAGE_UP)
PAGEDOWN_BUTTONS = (PAGE_DOWN, CTRL_PAGE_DOWN, ALT_PAGE_DOWN)

ARROW_CTRL = b'\x1b[1;5'
ARROW_ALT = b'\x1b[1;3'
ARROW_BASE = b'\x1b['
UP_ARROW = b'\x1b[A'
DOWN_ARROW = b'\x1b[B'
RIGHT_ARROW = b'\x1b[C'
LEFT_ARROW = b'\x1b[D'
CTRL_RIGHT_ARROW = b'\x1b[1;5C'
CTRL_LEFT_ARROW = b'\x1b[1;5D'

INSERT = b'\x1b[2~'
CTRL_ALT_INSERT = b'\x1b[2;7~'
CTRL_INSERT = b'\x1b[2;5~'
ALT_SHIFT_INSERT = b'\x1b[2;4~'
ALT_INSERT = b'\x1b[2;3~'
INSERTS = (INSERT, CTRL_ALT_INSERT, CTRL_INSERT, ALT_SHIFT_INSERT, ALT_INSERT)

DEL_KEY = b'\x1b[3~'
ALT_DEL = b'\x1b[3;3~'
CTRL_DEL = b'\x1b[3;5~'
DEL_KEYS = (DEL_KEY, CTRL_DEL, ALT_DEL)
SPECIAL_SYMBOLS = frozenset(' !@#$%^&*()_-+=\\|/,.~`<>{}[];:?"\'\t'.encode('ascii'))

print_lock = Lock()
bytes_rx, bytes_tx = Pipe()
data_pipe_rx, data_pipe_tx = Pipe()
predicted_part_drawn_size = Value('i', 0)


def endswith(string: bytes, ends: Iterable[bytes]) -> Optional[bytes]:
    predictor = PrefixTree(bytes(reversed(end)) for end in ends)
    result = predictor.get_prefixes(bytes(reversed(string)))
    if result:
        return bytes(reversed(max(result, key=lambda x: len(x))))


def write_log(logfile: BinaryIO, logfile_lock: Lock, line: bytes):
    logfile_lock.acquire()
    try:
        logfile.writelines((line, b'\r\n'))
        logfile.flush()
    finally:
        logfile_lock.release()


def get_time_string():
    now = datetime.now()
    return f"[{str(now.hour).rjust(2, '0')}:{str(now.minute).rjust(2, '0')}:"\
           f"{str(now.second).rjust(2, '0')}.{str(int(now.microsecond / 1000))[:3].rjust(3, '0')}] "


def read_file_input(
        file: Serial, logfile: Optional[BinaryIO], logfile_lock: Lock, msg_sender: Connection,
        input_line_transmitter: Connection, line_len_conn: Connection
):
    while True:
        try:
            raw_file_input = file.readline().rstrip(b'\r\n')
            file_input = raw_file_input.decode(errors='ignore')
            if file_input:
                line_len_conn.send(True)
                length = line_len_conn.recv() + MAX_CONTROL_SEQUENCE_SIZE
                print_lock.acquire()
                print('\r', end='')
                print(' ' * length, end='')
                print('\r', end='')
                print(text_styles.green(get_time_string() + file_input))
                print('\r', end='', flush=True)
                print_lock.release()
                input_line_transmitter.send(True)
                if logfile:
                    write_log(
                        logfile, logfile_lock,
                        f'{get_time_string()}recv: '.encode('ascii') + raw_file_input
                    )
        except SerialException:
            msg_sender.send(False)
            line_len_conn.send(False)
            input_line_transmitter.send(False)
            return


def prepare_line(line: bytes) -> bytes:
    for part in (UP_ARROW, DOWN_ARROW, RIGHT_ARROW, LEFT_ARROW, bytes((CONTROL_SYMBOL, ))):
        line = line.replace(part, b'')
    return line.strip(b'\r\n')


def redraw_line(
        line_to_print: bytes, currently_printed_line_size: int, caret_position: int,
        disable_invitation: bool = False, predicted_part: bytes = None
):
    print_lock.acquire()
    print('\r', end='')
    print(' ' * (
            currently_printed_line_size +
            MAX_CONTROL_SEQUENCE_SIZE +
            predicted_part_drawn_size.value
    ), end='', flush=True)
    print('\r' if disable_invitation else f'\r{INVITATION}', end='')
    line_to_print = prepare_line(line_to_print)
    if disable_invitation:
        print(get_time_string(), end='')
    print(
        line_to_print.decode('utf-8', errors='ignore'),
        end='', flush=True
    )
    if predicted_part:
        print(text_styles.grey(predicted_part.decode('utf-8', errors='ignore')), end='')
        print(chr(8) * len(predicted_part), flush=True, end='')
        predicted_part_drawn_size.value = len(predicted_part)
    else:
        predicted_part_drawn_size.value = 0
    if caret_position != -1:
        move_left = min(currently_printed_line_size, currently_printed_line_size - max(caret_position, 0))
        print(chr(8) * move_left, flush=True, end='')
    if disable_invitation:
        print('\r')
    print_lock.release()


def load_history() -> List[bytes]:
    filename = os.path.join(Path.home(), HISTORY_FILE_NAME)
    try:
        file = open(filename, 'rb')
    except (FileNotFoundError, PermissionError):
        return []
    try:
        return [prepare_line(item) for item in file.readlines() if item.strip(b'\r\n')]
    finally:
        file.close()


def save_history(history: List[bytes], history_size_at_start: int):
    filename = os.path.join(Path.home(), HISTORY_FILE_NAME)
    saved_history = load_history()
    try:
        file = open(filename, 'wb')
    except (FileNotFoundError, PermissionError):
        return
    try:
        history_size_to_save = len(history) - history_size_at_start
        history_to_save = [*saved_history, *(history[-history_size_to_save:])][-MAX_HISTORY_LINES:]
        file.writelines(prepare_line(item) + b'\n' for item in history_to_save if item.strip(b'\r\n'))
    finally:
        file.close()


def receive_bytes(stdin_fd: int):
    stdin_reopened = os.fdopen(stdin_fd, "rb", buffering=10240)
    tty.setraw(stdin_fd)
    try:
        while True:
            char = stdin_reopened.read(1)
            bytes_tx.send(char)
            if char == b'\x04':
                return
    except ValueError:
        bytes_tx.send(None)
        return
    finally:
        stdin_reopened.close()


def send_commands(
        file: Serial, logfile: Optional[BinaryIO], logfile_lock: Lock, milliseconds_delay: int,
        msg_sender: Connection, input_line_listener: Connection, input_line_transmitter: Connection,
        line_len_conn1: Connection, line_len_conn2: Connection, history_saver_rx: Connection
):
    line_to_execute = b''
    history = load_history()
    beginning_history_size = len(history)
    predictor = PrefixTree(set(history))
    caret_position = 0

    def redraw_predicted():
        if caret_position == len(line_to_execute) and caret_position != 0:
            predicted_part = (predictor.predict(line_to_execute) or b'')[len(line_to_execute):]
            redraw_line(
                line_to_execute, len(line_to_execute), -1, predicted_part=predicted_part
            )
        else:
            redraw_line(line_to_execute, len(line_to_execute), caret_position)

    def redraw_on_message():
        while input_line_listener.recv():
            if predicted_part_drawn_size.value:
                redraw_predicted()
            else:
                redraw_line(line_to_execute, len(line_to_execute), caret_position)

    def send_line_length():
        while line_len_conn2.recv():
            line_len_conn2.send(len(line_to_execute) + predicted_part_drawn_size.value)

    def history_saver():
        history_saver_rx.recv()
        save_history(history, beginning_history_size)
        history_saver_rx.send(None)

    def send_data():
        while True:
            line = data_pipe_rx.recv()
            start_time = datetime.now()
            if line is None:
                return
            file.write(line)
            file.flush()
            redraw_line(line, max(len(line), len(line_to_execute)) + len(INVITATION), -1, disable_invitation=True)
            redraw_line(line_to_execute, len(line_to_execute), caret_position)
            time_spent = (datetime.now() - start_time).microseconds / 1000
            for i in range(max(0, milliseconds_delay - int(time_spent + 1))):
                sleep(0.00099)

    line_len_sender = Thread(target=send_line_length)
    line_len_sender.start()
    history_saver_thread = Thread(target=history_saver)
    history_saver_thread.start()
    drawer = Thread(target=redraw_on_message)
    drawer.start()
    data_sender = Process(target=send_data, daemon=True)
    data_sender.start()
    try:
        while True:
            caret_position = 0
            input_line = b''
            line_to_execute = b''
            chosen_in_history: Optional[int] = None
            redraw_line(line_to_execute, len(line_to_execute), -1)
            while True:
                caret_position = min(len(line_to_execute), caret_position)
                current_char = bytes_rx.recv()
                if current_char is None:
                    raise ValueError
                if current_char == b'\x04':
                    raise EOFError
                if current_char == b'\x03':
                    len_of_line = len(line_to_execute)
                    caret_position = 0
                    input_line = b''
                    line_to_execute = b''
                    chosen_in_history: Optional[int] = None
                    redraw_line(line_to_execute, len_of_line, -1)
                    continue
                if current_char in b'\n\0':
                    continue
                if current_char == b'\r':
                    line_to_execute = line_to_execute.replace(b'\r', b'').replace(b'\n', b'')
                    redraw_line(line_to_execute, len(line_to_execute), -1)
                    break
                line_to_execute = line_to_execute[:caret_position] + current_char + line_to_execute[caret_position:]
                if chosen_in_history is None:
                    input_line = line_to_execute
                caret_position += 1
                caret_position = min(len(line_to_execute), caret_position)
                if ends_with_insert := endswith(line_to_execute[:caret_position], INSERTS):
                    line_to_execute = line_to_execute.replace(ends_with_insert, b'')
                    caret_position -= len(ends_with_insert)
                    if chosen_in_history is None:
                        input_line = line_to_execute
                    redraw_line(line_to_execute, len(line_to_execute), caret_position)
                    continue
                if line_to_execute[:caret_position].endswith(CTRL_LEFT_ARROW):
                    caret_position -= len(CTRL_LEFT_ARROW)
                    line_to_execute = line_to_execute.replace(CTRL_LEFT_ARROW, b'')
                    if chosen_in_history is None:
                        input_line = line_to_execute
                    while caret_position > 0 and line_to_execute[caret_position - 1] in SPECIAL_SYMBOLS:
                        caret_position -= 1
                    while caret_position > 0 and line_to_execute[caret_position - 1] not in SPECIAL_SYMBOLS:
                        caret_position -= 1
                    redraw_line(line_to_execute, len(line_to_execute), caret_position)
                    continue
                if line_to_execute[:caret_position].endswith(CTRL_RIGHT_ARROW):
                    caret_position -= len(CTRL_RIGHT_ARROW)
                    line_to_execute = line_to_execute.replace(CTRL_RIGHT_ARROW, b'')
                    if chosen_in_history is None:
                        input_line = line_to_execute
                    while caret_position < len(line_to_execute) and line_to_execute[caret_position] in SPECIAL_SYMBOLS:
                        caret_position += 1
                    while caret_position < len(line_to_execute) and \
                            line_to_execute[caret_position] not in SPECIAL_SYMBOLS:
                        caret_position += 1
                    redraw_predicted()
                    continue
                ends_with_alt = endswith(line_to_execute[:caret_position - 1], (ARROW_CTRL, ARROW_ALT))
                if ends_with_alt:
                    len_of_line = len(line_to_execute)
                    line_to_execute = line_to_execute.replace(ends_with_alt, ARROW_BASE)
                    if chosen_in_history is None:
                        input_line = line_to_execute
                    caret_position -= abs(len(ends_with_alt) - len(ARROW_BASE))
                    caret_position = max(caret_position, 0)
                    redraw_line(line_to_execute, len_of_line, caret_position)
                if line_to_execute[:caret_position].endswith(SHIFT_ENTER):
                    line_to_execute = line_to_execute.replace(SHIFT_ENTER, b'')
                    redraw_line(line_to_execute, len(line_to_execute), -1)
                    break
                if line_to_execute[:caret_position].endswith(HOME_BTN):
                    line_to_execute = line_to_execute.replace(HOME_BTN, b'')
                    if chosen_in_history is None:
                        input_line = line_to_execute
                    caret_position = 0
                    redraw_line(line_to_execute, len(line_to_execute), caret_position)
                elif line_to_execute[:caret_position].endswith(END_BTN):
                    line_to_execute = line_to_execute.replace(END_BTN, b'')
                    caret_position = len(line_to_execute)
                    if chosen_in_history is None:
                        input_line = line_to_execute
                    redraw_line(line_to_execute, len(line_to_execute), -1)
                elif ends_with_pageup := endswith(line_to_execute[:caret_position], PAGEUP_BUTTONS):
                    line_to_execute = line_to_execute.replace(ends_with_pageup, b'')
                    len_of_line = len(line_to_execute)
                    if chosen_in_history is None:
                        input_line = line_to_execute
                    if chosen_in_history == -len(history):
                        redraw_line(line_to_execute, len_of_line, caret_position)
                        continue
                    if chosen_in_history is None:
                        if history:
                            chosen_in_history = -10
                            line_to_execute = history[chosen_in_history]
                            caret_position = len(line_to_execute)
                    else:
                        chosen_in_history -= 10
                        chosen_in_history = max(-len(history), chosen_in_history)
                        line_to_execute = history[chosen_in_history]
                    caret_position = len(line_to_execute)
                    redraw_line(line_to_execute, len_of_line, -1)
                elif ends_with_pagedown := endswith(line_to_execute[:caret_position], PAGEDOWN_BUTTONS):
                    line_to_execute = line_to_execute.replace(ends_with_pagedown, b'')
                    len_of_line = len(line_to_execute)
                    if chosen_in_history is None:
                        redraw_line(line_to_execute, len_of_line, caret_position)
                        continue
                    chosen_in_history += 10
                    if chosen_in_history >= 0:
                        chosen_in_history = None
                        line_to_execute = input_line
                    else:
                        line_to_execute = history[chosen_in_history]
                    caret_position = len(line_to_execute)
                    redraw_line(line_to_execute, len_of_line, -1)
                elif line_to_execute[:caret_position].endswith(LEFT_ARROW):
                    caret_position -= len(LEFT_ARROW) + 1
                    caret_position = max(0, caret_position)
                    line_to_execute = line_to_execute.replace(LEFT_ARROW, b'')
                    if chosen_in_history is None:
                        input_line = line_to_execute
                    redraw_line(line_to_execute, len(line_to_execute), caret_position)
                elif line_to_execute[:caret_position].endswith(RIGHT_ARROW):
                    caret_position -= len(RIGHT_ARROW) - 1
                    line_to_execute = line_to_execute.replace(RIGHT_ARROW, b'')
                    caret_position = min(len(line_to_execute), caret_position)
                    if chosen_in_history is None:
                        input_line = line_to_execute
                    redraw_line(line_to_execute, len(line_to_execute), caret_position)
                elif line_to_execute[:caret_position].endswith(UP_ARROW):
                    len_of_line = len(line_to_execute)
                    line_to_execute = line_to_execute.replace(UP_ARROW, b'')
                    if chosen_in_history is None:
                        input_line = input_line.replace(UP_ARROW, b'')
                        if history:
                            chosen_in_history = -1
                            line_to_execute = history[chosen_in_history]
                            caret_position = len(line_to_execute)
                    else:
                        try:
                            chosen_in_history -= 1
                            line_to_execute = history[chosen_in_history]
                        except IndexError:
                            chosen_in_history += 1
                        finally:
                            caret_position = len(line_to_execute)
                    redraw_line(line_to_execute, len_of_line, -1)
                elif line_to_execute[:caret_position].endswith(DOWN_ARROW):
                    len_of_line = len(line_to_execute)
                    line_to_execute = line_to_execute.replace(DOWN_ARROW, b'')
                    if chosen_in_history is not None:
                        if chosen_in_history == -1:
                            line_to_execute = input_line
                            chosen_in_history = None
                        else:
                            chosen_in_history += 1
                            line_to_execute = history[chosen_in_history]
                        caret_position = len(line_to_execute)
                    redraw_line(line_to_execute, len_of_line, -1)
                elif line_to_execute[:caret_position] and line_to_execute[:caret_position][-1] == CTRL_L:
                    line_to_execute = line_to_execute[:caret_position - 1] + line_to_execute[caret_position:]
                    caret_position -= 1
                    caret_position = max(0, caret_position)
                    if chosen_in_history is not None:
                        input_line = line_to_execute
                elif line_to_execute[:caret_position] and \
                        line_to_execute[:caret_position][-1] in (BACKSPACE, BACKSPACE_CTRL):
                    bs_symbol = line_to_execute[:caret_position][-1]
                    line_to_execute = line_to_execute.replace(bytes((bs_symbol, )), b'')
                    caret_position -= 1
                    caret_position = max(0, caret_position)
                    if line_to_execute[:caret_position] and line_to_execute[:caret_position][-1] == CONTROL_SYMBOL:
                        line_to_execute = line_to_execute.replace(bytes((CONTROL_SYMBOL,)), b'')
                        caret_position -= 1
                        caret_position = max(0, caret_position)
                    len_of_line = len(line_to_execute)
                    if bs_symbol == BACKSPACE:
                        if line_to_execute[:caret_position]:
                            line_to_execute = line_to_execute[:caret_position - 1] + line_to_execute[caret_position:]
                            caret_position -= 1
                            caret_position = max(0, caret_position)
                    else:
                        runner = caret_position
                        while runner > 0 and line_to_execute[runner - 1] in SPECIAL_SYMBOLS:
                            runner -= 1
                        while runner > 0 and line_to_execute[runner - 1] not in SPECIAL_SYMBOLS:
                            runner -= 1
                        line_to_execute = line_to_execute[:runner] + line_to_execute[caret_position:]
                        caret_position = runner
                    if chosen_in_history is None:
                        input_line = line_to_execute
                    redraw_line(line_to_execute, len_of_line, caret_position)
                elif ends_with_del := endswith(line_to_execute[:caret_position], DEL_KEYS):
                    len_of_line = len(line_to_execute)
                    line_to_execute = line_to_execute[:caret_position - len(ends_with_del)] +\
                        line_to_execute[caret_position:]
                    caret_position -= len(ends_with_del)
                    caret_position = max(0, caret_position)
                    if ends_with_del == DEL_KEY and line_to_execute[caret_position:]:
                        line_to_execute = line_to_execute[:caret_position] + line_to_execute[caret_position + 1:]
                    elif ends_with_del == ALT_DEL and line_to_execute[caret_position:]:
                        line_to_execute = line_to_execute[:caret_position] + line_to_execute[caret_position + 1:]
                    elif ends_with_del == CTRL_DEL:
                        runner = caret_position
                        while runner < len(line_to_execute) and line_to_execute[runner] in SPECIAL_SYMBOLS:
                            runner += 1
                        while runner < len(line_to_execute) and line_to_execute[runner] not in SPECIAL_SYMBOLS:
                            runner += 1
                        line_to_execute = line_to_execute[:caret_position] + line_to_execute[runner:]
                    if chosen_in_history is not None:
                        input_line = line_to_execute
                    redraw_line(line_to_execute, len_of_line, caret_position)
                else:
                    if line_to_execute.endswith(b'\t'):
                        line_to_execute = line_to_execute.rstrip(b'\t')
                        len_of_line = len(line_to_execute)
                        if line_to_execute:
                            line_to_execute = predictor.predict(line_to_execute) or line_to_execute
                        caret_position = len(line_to_execute)
                        if chosen_in_history is None:
                            input_line = line_to_execute
                        if line_to_execute:
                            predicted_line_part = (predictor.predict(line_to_execute) or b'')[len(line_to_execute):]
                            redraw_line(line_to_execute, len_of_line, -1, predicted_part=predicted_line_part)
                        else:
                            redraw_line(line_to_execute, len(line_to_execute), -1)
                    else:
                        if line_to_execute[:caret_position].endswith(b'\t'):
                            line_to_execute = line_to_execute.replace(b'\t', b'')
                            caret_position -= 1
                            caret_position = max(0, caret_position)
                        if chosen_in_history is None:
                            input_line = line_to_execute
                redraw_predicted()
            if line_to_execute[:caret_position] and line_to_execute[:caret_position][-1] == CONTROL_SYMBOL:
                line_to_execute = line_to_execute.replace(bytes((CONTROL_SYMBOL, )), b'')
                line_to_execute += calculate_checksum(line_to_execute)
            if line_to_execute:
                if not history or history[-1] != line_to_execute:
                    history.append(line_to_execute)
                    predictor.update(line_to_execute)
                if logfile:
                    write_log(
                        logfile, logfile_lock,
                        f'{get_time_string()}send: '.encode('ascii') + line_to_execute
                    )
            data_pipe_tx.send(line_to_execute + b'\r\n')
            redraw_line(b'', len(line_to_execute), -1)
    except (EOFError, KeyboardInterrupt):
        msg_sender.send(True)
        return
    except ValueError:
        msg_sender.send(False)
        return
    finally:
        data_pipe_tx.send(None)
        history_saver_thread.join()
        line_len_conn1.send(False)
        input_line_transmitter.send(False)
        line_len_sender.join()
        drawer.join()
        data_sender.kill()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("path", help="path to serial port file")
    parser.add_argument("-b", "--baudrate", help="baudrate of the serial port", type=int, default=9600)
    parser.add_argument("-l", "--logfile", help="file to write logs into", type=str, default=None)
    parser.add_argument("-d", "--delay", help="delay between sent lines, in milliseconds", type=int, default=0)
    args = parser.parse_args()
    if not os.path.exists(args.path):
        print(text_styles.red(f"Can not find '{args.path}' file"))
        exit(1)
    file = None
    logfile = None
    if args.logfile:
        try:
            logfile = open(args.logfile, 'wb')
        except PermissionError:
            print(text_styles.red(f"Can not open '{args.path}' file for logs"))
            exit(1)
    try:
        file = Serial(args.path, baudrate=args.baudrate)
    except PermissionError:
        print(text_styles.red(f"Can not open '{args.path}' (permission denied)"))
        exit(1)
    except SerialException:
        print(text_styles.red(f"Can not open '{args.path}'"))
        exit(1)
    print(text_styles.yellow(
        "Hint: Use Alt+Enter to send the command with its checksum (it will be generated automatically)"
    ))
    receiver, transmitter = Pipe()
    input_msg_receiver, input_msg_transmitter = Pipe()
    line_len_conn1, line_len_conn2 = Pipe()
    history_saver_rx, history_saver_tx = Pipe()
    logfile_lock = Lock()
    transmitting_process = Process(target=send_commands, args=(
        file, logfile, logfile_lock, args.delay, transmitter, input_msg_receiver,
        input_msg_transmitter, line_len_conn1, line_len_conn2, history_saver_rx
    ))
    receiving_process = Process(target=read_file_input, args=(
        file, logfile, logfile_lock, transmitter, input_msg_transmitter, line_len_conn1
    ), daemon=True)
    bytes_receiver = Process(target=receive_bytes, args=(stdin.fileno(), ), daemon=True)
    stdin_buff_fd = stdin.buffer.fileno()
    old_settings = termios.tcgetattr(stdin_buff_fd)
    bytes_receiver.start()
    receiving_process.start()
    transmitting_process.start()
    try:
        exited_correctly = receiver.recv()
        data_pipe_tx.send(None)
        history_saver_tx.send(None)
        history_saver_tx.recv()
        if exited_correctly:
            transmitting_process.join()
            print(text_styles.yellow("\n\rExit\r"))
        else:
            transmitting_process.terminate()
            print(text_styles.red("\n\rSerial port was closed, exiting\r"))
    except (KeyboardInterrupt, EOFError):
        transmitting_process.terminate()
        raise
    finally:
        receiving_process.terminate()
        bytes_receiver.terminate()
        termios.tcsetattr(stdin_buff_fd, termios.TCSADRAIN, old_settings)
        file.close()


if __name__ == "__main__":
    try:
        main()
    except (EOFError, KeyboardInterrupt):
        print(text_styles.yellow("\n\rExit\r"))
        exit()
