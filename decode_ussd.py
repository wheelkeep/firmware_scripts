def decode_ussd_hex_codes(code: str) -> str:
    return bytes([
        int(i, 16) for i in (
            f"{code[j]}{code[j+1] if len(code) > j else '0'}" for j in range(0, len(code), 2)
        ) if i != '00'
    ]).decode('ascii', errors='ignore')


if __name__ == "__main__":
    print(f'\n{decode_ussd_hex_codes(input("Enter the hex data: "))}')
