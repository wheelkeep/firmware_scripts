"""This script generates random ID and Key of the device and makes a file of 64 KiB size which contains it.

Usage:
    python ./generate_dik.py /path/to/incomplete/firmware.bin /path/to/output/file.bin

At first, 62 (or 126) KiB will be copied from source file to the destination one.
If the input file is less than 62 (or 126) KiB, it will copy all its content and
fill remaining space with 0xFF up to 62 (or 126) KiB.
Then 2 pages with will be generated with ID and Key in each of them.
In the end these 2 pages will be written to the output file and it will grow up to 64 (or 128) KiB.
"""

from typing import BinaryIO
import argparse
import sys
import os

from utils import text_styles
from lock_token_gen import generate_random_token


DEFAULT_BYTE_VALUE = 0xFF


def generate_random_dik() -> bytes:
    name = generate_random_token(8)
    print(f"Device ID: {text_styles.bold(text_styles.cyan(name))}")
    return f'{name}\0{generate_random_token(32)}\0'.encode('ascii')


def complete_firmware_file(
        output_file: BinaryIO, input_file: BinaryIO, input_file_size: int, pages_count: int, page_size: int
) -> None:
    input_size = min((pages_count - 2) * page_size, input_file_size)
    pages_to_copy = (input_size // page_size) + int(input_size % page_size != 0)
    input_file.seek(0)
    output_file.seek(0)
    for _ in range(pages_to_copy):
        output_file.write(input_file.read(page_size))
    for _ in range((pages_count - 2) * page_size - input_size):
        output_file.write(bytes((DEFAULT_BYTE_VALUE, )))
    dick_to_write = generate_random_dik()
    for _ in range(2):
        output_file.write(dick_to_write)
        output_file.write(bytes(DEFAULT_BYTE_VALUE for _ in range(max(0, page_size - len(dick_to_write)))))


def try_open_output_file(input_file_stat, input_file_name: str, output_file_name: str) -> BinaryIO:
    try:
        output_file = open(output_file_name)
        output_stat = os.fstat(output_file.fileno())
        if input_file_stat.st_ino == output_stat.st_ino and input_file_stat.st_dev == output_stat.st_dev:
            print(text_styles.red(f"Error: '{input_file_name}' and '{output_file_name}' are the same file"))
            exit(1)
        output_file.close()
    except FileNotFoundError:
        pass
    return open(output_file_name, 'wb')


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument("input_file", help="path to incomplete binary firmware file")
    parser.add_argument("output_file", help="path to output binary firmware file")
    parser.add_argument("-c", "--pages_count", help="pages count that should be in output file", type=int, default=64)
    parser.add_argument("-s", "--page_size", help="size of flash page (in bytes)", type=int, default=1024)
    parser.add_argument("-l", "--input_read_limit", help="limit of pages to read from input file", type=int, default=0)
    args = parser.parse_args()
    if not os.path.isfile(args.input_file):
        print(text_styles.red(f"Can not find '{args.input_file}' file"), file=sys.stderr)
        exit(1)
    input_file = None
    input_stat = None
    try:
        input_file = open(args.input_file, 'rb')
        input_stat = os.fstat(input_file.fileno())
    except PermissionError:
        print(text_styles.red(f"Can not open '{args.input_file}' (permission denied)"), file=sys.stderr)
        exit(1)
    output_file = None
    if os.path.isfile(args.output_file):
        print(
            f"Output file '{text_styles.bold(args.output_file)}' already exists. Would you like to rewrite it? [Y/n] ",
            end=''
        )
        if input() in ('', 'Y', 'y'):
            try:
                output_file = try_open_output_file(input_stat, args.input_file, args.output_file)
            except PermissionError:
                print(text_styles.red(f"Can not open '{args.output_file}' (permission denied)"), file=sys.stderr)
                exit(1)
        else:
            print(text_styles.yellow("Aborted"))
            exit()
    else:
        try:
            os.makedirs(os.path.dirname(args.output_file) or '.', exist_ok=True)
            output_file = try_open_output_file(input_stat, args.input_file, args.output_file)
        except (FileNotFoundError, PermissionError):
            print(text_styles.red(f"Can not open file '{args.output_file}' for writing"))
            exit(1)
        except IsADirectoryError:
            print(text_styles.red(f"'{args.output_file}' is a directory"))
            exit(1)
    input_file_size = input_stat.st_size
    complete_firmware_file(
        output_file, input_file,
        min(input_file_size, args.input_read_limit * args.page_size) if args.input_read_limit else input_file_size,
        args.pages_count, args.page_size
    )
    print(text_styles.green("Done!"))


if __name__ == "__main__":
    try:
        main()
    except (EOFError, KeyboardInterrupt):
        print(text_styles.yellow("\nExit"))
