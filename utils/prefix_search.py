from typing import Iterable, AnyStr, Union, Optional, Dict, Set


class PrefixTree:
    class PrefixTreeNode:
        def __init__(self, value: Optional[Union[AnyStr, int]], finite: bool):
            self.transitions: Dict[Union[AnyStr, int], 'PrefixTree.PrefixTreeNode'] = {}
            self.value = value
            self.finite = finite

        def __repr__(self):
            return f"<PrefixTreeNode(value={self.value}, finite={self.finite})>"

    def __init__(self, strings: Iterable[AnyStr]):
        self.root = PrefixTree.PrefixTreeNode(None, False)
        for i in strings:
            if isinstance(i, str):
                i = i.encode(errors='ignore')
            self.update(i)

    def update(self, string: AnyStr):
        if isinstance(string, str):
            string = string.encode(errors='ignore')
        current_node = self.root
        for i in range(len(string)):
            if string[i] in current_node.transitions:
                current_node = current_node.transitions[string[i]]
                if i == (len(string) - 1):
                    current_node.finite = True
            else:
                new_node = self.PrefixTreeNode(string[i], i == (len(string) - 1))
                current_node.transitions[string[i]] = new_node
                current_node = new_node

    def predict(self, prefix: AnyStr) -> Optional[AnyStr]:
        if isinstance(prefix, str):
            prefix_encoded = prefix.encode(errors='ignore')
        else:
            prefix_encoded = prefix
        current_node = self.root
        for i in prefix_encoded:
            if i in current_node.transitions:
                current_node = current_node.transitions[i]
            else:
                return None
        result = list(prefix_encoded)
        while current_node.transitions:
            current_node = current_node.transitions[next(iter(current_node.transitions))]
            result.append(current_node.value)
            if current_node.finite:
                break
        result = bytes(result)
        if isinstance(prefix, str):
            result = result.decode(errors='ignore')
        return result

    def get_prefixes(self, string: AnyStr) -> Set[AnyStr]:
        if isinstance(string, str):
            string_encoded = string.encode(errors='ignore')
        else:
            string_encoded = string
        current_node = self.root
        result = set()
        current_prefix = []
        for i in string_encoded:
            if i not in current_node.transitions:
                break
            current_node = current_node.transitions[i]
            current_prefix.append(current_node.value)
            if current_node.finite:
                value_to_append = bytes(current_prefix)
                if isinstance(string, str):
                    value_to_append = value_to_append.decode(errors='ignore')
                result.add(value_to_append)
        return result
