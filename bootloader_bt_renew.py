import os
import sys
import argparse
import binascii
from hashlib import sha512
from base64 import b64encode, b64decode
from typing import BinaryIO, Optional, Union, Generator, Iterable
from enum import Enum

from serial import Serial, SerialException

from utils import text_styles


TIMEOUT = 10  # seconds
MAX_COMMAND_ATTEMPTS = 8
MESSAGE_MAX_SIZE = 1024
BYTES_PER_MESSAGE = 64


class RenewerState(Enum):
    not_started = 0
    writing_data = 1
    bootloader_confirmed = 2
    version_written = 3


class MessageType(Enum):
    binary = 0
    text = 1


class SynchronizationError(Exception):
    pass


class BlRenewer:
    def __init__(self):
        self.state: RenewerState = RenewerState.not_started
        self.bytes_written: int = 0
        self.serial_interactor: Optional[Serial] = None

    @property
    def is_connected(self) -> bool:
        return bool(self.serial_interactor) and not self.serial_interactor.closed

    def connect(self, serial_port: str):
        if self.is_connected:
            self.serial_interactor.close()
        self.serial_interactor = Serial(port=serial_port, baudrate=9600, timeout=TIMEOUT)

    def disconnect(self):
        self.serial_interactor.close()
        self.serial_interactor = None

    @staticmethod
    def decode_response(response: str) -> Union[str, bytes]:
        response = response.strip('\r\n')
        if len(response) < 3:
            raise ValueError("Message is too short")
        checksum = response[-2:].lower()
        real_checksum = hex(sum(response[:-2].encode('ascii', errors='ignore')) % 256)[2:].rjust(2, '0').lower()
        if checksum != real_checksum:
            raise ValueError("Invalid checksum")
        message_type = MessageType(int(response[0]))  # it will throw a ValueError on error so that's ok
        response_body = response[1:-2]
        if message_type == MessageType.binary:
            try:
                return b64decode(response_body)
            except binascii.Error:
                raise ValueError("Invalid b64 data")
        elif message_type == MessageType.text:
            return response_body

    def send_command(
            self, command: str, simple: bool = True, stop_values: Iterable[str] = tuple()
    ) -> Union[str, Generator[str, None, None]]:
        if not self.is_connected:
            raise ConnectionError("not connected to serial device")
        prepared_command = f"1{command}"
        checksum = hex(sum(prepared_command.encode("ascii", errors="ignore")) % 256)[2:]
        prepared_command += f"{checksum}\r\n"
        self.serial_interactor.reset_input_buffer()
        return self._send_prepared_command(prepared_command, simple=simple, stop_values=stop_values)

    def _send_prepared_command(
            self, prepared_command, simple: bool = True, stop_values: Iterable[str] = tuple()
    ) -> Union[str, Generator[str, None, None]]:
        if len(prepared_command) > 1024:
            raise ValueError("Message is too long")

        def send_and_read_response():
            for j in range(MAX_COMMAND_ATTEMPTS):
                should_continue = False
                self.serial_interactor.write(prepared_command.encode("ascii", errors="ignore"))
                for _ in range(1 if simple else 2):
                    raw_response = self.serial_interactor.read_until(b'\n').decode(errors='ignore')
                    try:
                        response = self.decode_response(raw_response)
                    except ValueError:
                        should_continue = True
                        break
                    if response in ("message_parse_error", "checksum_error", "message_type_error"):
                        should_continue = True
                        break
                    if response == "command not found":
                        raise RuntimeError(f'Invalid command ("{prepared_command}") was sent to the device')
                    if simple:
                        yield response
                        return
                    else:
                        yield response
                        if response in stop_values:
                            return
                if not should_continue:
                    break
                if j == MAX_COMMAND_ATTEMPTS - 1:
                    raise RuntimeError("Failed to send a command: can not understand device responses")

        result = send_and_read_response()
        if simple:
            return result.send(None)
        else:
            return result

    def synchronize_state(self):
        state = self.send_command("state", simple=True)
        if state == "not started":
            self.bytes_written = 0
            self.state = RenewerState.not_started
        elif state.startswith("written ") and state.endswith(" bytes"):
            self.bytes_written = int(state.split()[1])
            self.state = RenewerState.writing_data
        elif state == "no version":
            self.state = RenewerState.bootloader_confirmed
            self.bytes_written = 0
        elif state == "done":
            self.state = RenewerState.version_written
            self.bytes_written = 0
        else:
            raise ValueError("Invalid state received from the device")

    def _send_binary_data(self, data: bytes):
        if self.state != RenewerState.writing_data:
            raise SynchronizationError("Tried to write data in incorrect state")
        message = f"0{b64encode(data).decode('ascii')}"
        checksum = hex(sum(message.encode("ascii", errors="ignore")) % 256)[2:]
        message += f"{checksum}\r\n"
        msg_send_result = self._send_prepared_command(message, simple=False, stop_values={"state error"})
        if msg_send_result.send(None) == "state error":
            raise SynchronizationError("Tried to write data in incorrect state")
        if msg_send_result.send(None) == "no place left":
            self.synchronize_state()
            raise RuntimeError("No place left in the bootloader flash area")
        self.bytes_written += len(data)

    def prepare_for_flashing(self):
        if self.state != RenewerState.not_started:
            for i in self.send_command("restart", simple=False, stop_values={"not started"}):
                if i == "not started":
                    self.synchronize_state()
        else:
            for i in self.send_command("start", simple=False, stop_values={"already started"}):
                if i == "already started":
                    self.synchronize_state()
                    self.prepare_for_flashing()
        self.state = RenewerState.writing_data
        self.bytes_written = 0

    def send_binary_file(self, file: BinaryIO) -> Generator[Union[int, str], None, None]:
        if self.state != RenewerState.writing_data:
            raise SynchronizationError("Invalid state")
        bootloader_checksum = sha512()
        if self.bytes_written != 0:
            if not file.seekable():
                raise RuntimeError("Can not calculate the checksum for the bootloader, because file is not seekable")
            file.seek(0)
            bytes_hashed = 0
            while bytes_hashed != self.bytes_written:
                bytes_to_hash = file.read(min(BYTES_PER_MESSAGE, self.bytes_written - bytes_hashed))
                bootloader_checksum.update(bytes_to_hash)
                bytes_hashed += len(bytes_to_hash)
        yield self.bytes_written
        while data_to_send := file.read(BYTES_PER_MESSAGE):
            self._send_binary_data(data_to_send)
            bootloader_checksum.update(data_to_send)
            yield self.bytes_written
        yield "Finishing..."
        finish_response = self.send_command(f"finish {bootloader_checksum.hexdigest()}", simple=False, stop_values={
                "bootloader empty", "already finished", "invalid checksum given"
        })
        part1 = finish_response.send(None)
        if part1 == "bootloader empty":
            raise ValueError("Nothing was written, probably due to an empty file")
        elif part1 == "already finished":
            raise SynchronizationError("Already finished")
        elif part1 == "invalid checksum given":
            raise RuntimeError(part1)
        elif part1 != "validating":
            raise RuntimeError(f"Unknown response received: {part1}")
        part2 = finish_response.send(None)
        if part2 == "bootloader checksum invalid":
            raise ValueError("Checksum does not match")
        elif part2 == "bootloader header invalid":
            raise ValueError("Given binary file does not contain a valid bootloader")
        elif part2 != "finished":
            raise RuntimeError(f"Unknown response received: {part2}")
        self.state = RenewerState.bootloader_confirmed

    def set_bootloader_version(self, version: str):
        if self.state != RenewerState.bootloader_confirmed and self.state != RenewerState.version_written:
            raise SynchronizationError("Invalid state")
        version_setting_result = self.send_command(f"set version {version}", simple=False, stop_values={"unavailable"})
        if version_setting_result.send(None) == "unavailable":
            self.synchronize_state()
            raise SynchronizationError("Invalid state")
        if version_setting_result.send(None) == "version invalid":
            raise ValueError("Invalid version given")
        self.state = RenewerState.version_written

    def get_bootloader_version(self) -> str:
        return self.send_command("get version", simple=True)

    def reboot_device(self, safe: bool):
        if safe:
            if self.send_command("reboot safe", simple=True) == "reboot error":
                raise SynchronizationError("Can not reboot in current state")
        else:
            for i in self.send_command("reboot unsafe", simple=False, stop_values={"reboot error"}):
                if i == "reboot error":
                    raise SynchronizationError("Can not reboot in current state")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("path", help="path to bootloader binary file")
    parser.add_argument("port", help="COM port to communicate with")
    args = parser.parse_args()
    input_file = None
    if not os.path.isfile(args.path):
        print(text_styles.red(f"Can not find '{args.path}' file"))
        exit(1)
    try:
        input_file = open(args.path, 'rb')
    except PermissionError:
        print(text_styles.red(f"Can not open '{args.path}' (permission denied)"))
        exit(1)
    updater = BlRenewer()
    try:
        updater.connect(args.port)
    except SerialException:
        print(f'{text_styles.red("Can not connect to serial port")} {args.port}')
        exit(1)

    def erase_printed_letters(n: int):
        print(chr(8) * n, end='')
        print(' ' * n, end='', flush=True)
        print(chr(8) * n, end='')

    def send_file_and_print():
        for state in updater.send_binary_file(input_file):
            erase_printed_letters(100)
            if isinstance(state, int):
                print(f"{state} bytes sent", end='', flush=True)

            else:
                print(text_styles.yellow(state), end='', flush=True)
        erase_printed_letters(100)

    def input_version():
        updater.set_bootloader_version(input("Enter the bootloader version: "))

    def device_reboot_dialog():
        updater.reboot_device(safe=(input("Erase the Cobra marker before reboot? [Y/n] ") not in ('', 'Y', 'y')))

    updater.synchronize_state()
    if not input_file.seekable():
        continue_from_current = False
    else:
        if updater.state != RenewerState.not_started:
            print("Flashing of the bootloader has already been started before.")
            print("Do you want to resume from current state?")
            print("Otherwise the flashing will be restarted [Y(resume)/n(restart)] ", end='')
            answer = input()
            if answer in ('', 'Y', 'y'):
                continue_from_current = True
            else:
                continue_from_current = False
        else:
            continue_from_current = False

    if continue_from_current:
        if updater.state == RenewerState.writing_data:
            send_file_and_print()
            input_version()
            device_reboot_dialog()
        elif updater.state == RenewerState.bootloader_confirmed:
            print("Seems like bootloader is already bootable, but there's no it's version")
            input_version()
            device_reboot_dialog()
        elif updater.state == RenewerState.version_written:
            print(
                "Bootloader was completely written before, current version is "
                f"'{text_styles.blue(updater.get_bootloader_version())}'"
            )
            device_reboot_dialog()
    else:
        updater.prepare_for_flashing()
        send_file_and_print()
        input_version()
        device_reboot_dialog()

    updater.disconnect()
    print(text_styles.green("Done"))


if __name__ == "__main__":
    try:
        main()
    except (EOFError, KeyboardInterrupt):
        print(text_styles.yellow("\nExit"))
        exit()
    except (SerialException, ConnectionError):
        print(text_styles.red('\nConnection lost'))
        exit(1)
    except (ValueError, RuntimeError, SynchronizationError) as exc:
        print(text_styles.red(f'\n{type(exc).__name__}: {exc}'))
        exit(1)
