"""This script reads device logs from file (in binary representation) and prints them out in human-readable format.
usage: read_logs.py [-h] [-o OFFSET] [-a AMOUNT] path
"""

import os
import sys
import argparse
from math import inf, isinf
from enum import Enum
from typing import Final, List, Dict, Any, BinaryIO, Optional

from utils import text_styles


class LogStructure:
    LOG_SIZE: Final = 4  # bytes
    LOG_BITS_TOTAL: Final = LOG_SIZE * 8
    CONTROL_BIT: Final = 0
    CONTROL_BIT_VALUE: Final = 0  # it should always be 0
    CHARGE_START: Final = 1
    CHARGE_SIZE: Final = 7
    TYPE_START: Final = 8
    TYPE_SIZE: Final = 2
    SOURCE_START: Final = 10
    SOURCE_SIZE: Final = 3
    CODE_START: Final = 13
    CODE_SIZE: Final = 3
    TIME_START: Final = 16
    TIME_SIZE: Final = 16


class LogType(Enum):
    notification = 0
    warning = 1
    error = 2
    critical_error = 3


class LogSource(Enum):
    wk = 0
    mode = 1
    flash = 2
    api = 3
    gsm = 4
    gps = 5
    imu = 6


LOG_MESSAGES = {
    LogSource.wk: [
        "Device init",
        "Debug mode",
        "Default mode",
        "Started binding",
        "Started unbinding",
        "Cobra Restart"
    ],
    LogSource.mode: [
        "Mode Locked",
        "Mode Unlocked",
        "Mode Wait for user action",
        "Mode Post-user action",
        "Mode Stolen",
        "Mode Reboot",
        "Mode Sleep",
        "Mode Awake"
    ],
    LogSource.gps: [
        "Got fix",
        "Ass isted happened"
    ],
    LogSource.gsm: [
        "Init failed",
        "Can't connect to network",
        "Request send timeout",
        "Status Error",
        "Connection to server lost during sending request",
        "Network lost during sending request",
        "Can’t detect a SIM-card"
    ],
    LogSource.imu: [
        "Movement detected",
        "'Who am I' check failed"
    ],
    LogSource.api: [
        "Can't connect to server",
        "Request sent successfully",
        "Device registered",
        "Fix coordinates sent",
        "Can’t register/authorize"
    ],
    LogSource.flash: [
        "No valid device_id and device_key found in flash!",
        "No valid device_id found in flash!",
        "No valid device_key found in flash!",
        "Logs page deleted before sending to server!",
        "No bootloader found! Setting up the stub…",
        "There is a stub instead of the bootloader",
        "Bootloader seems to be corrupted. Restarting the cobra itself"
    ]
}


def get_bitmask(size: int) -> int:
    result = 0
    for _ in range(size):
        result <<= 1
        result |= 1
    return result


def parse_logs(raw_logs: bytes) -> List[Dict[str, Any]]:
    result = []
    for i in range(len(raw_logs) // LogStructure.LOG_SIZE):
        current_log = int.from_bytes(raw_logs[i * 4: (i + 1) * 4], byteorder="little")
        if (current_log & 1) != LogStructure.CONTROL_BIT_VALUE:
            result.append({"error": "incorrect control bit"})
            continue
        log_type = (current_log >> LogStructure.TYPE_START) & get_bitmask(LogStructure.TYPE_SIZE)
        source = (current_log >> LogStructure.SOURCE_START) & get_bitmask(LogStructure.SOURCE_SIZE)
        message_number = (current_log >> LogStructure.CODE_START) & get_bitmask(LogStructure.CODE_SIZE)
        charge = (current_log >> LogStructure.CHARGE_START) & get_bitmask(LogStructure.CHARGE_SIZE)
        time_bitmask = get_bitmask(LogStructure.TIME_SIZE)
        time = (current_log >> LogStructure.TIME_START) & time_bitmask
        if time == time_bitmask:
            time = inf

        current_item = {}
        try:
            messages_key = LogSource(source)
            current_item["source"] = messages_key.name
            if message_number < len(LOG_MESSAGES.get(messages_key, tuple())):
                current_item["message"] = LOG_MESSAGES[messages_key][message_number]
            else:
                current_item["message"] = None
        except ValueError:
            current_item["source"] = None
        try:
            current_item["type"] = LogType(log_type).name
        except ValueError:
            current_item["type"] = None
        current_item["charge"] = charge if charge in range(101) else None
        current_item["time"] = time / 100
        result.append(current_item)
    return result


def read_logs_from_file(file: BinaryIO, offset: int, logs_amount: Optional[int]) -> bytes:
    file.seek(offset)
    return file.read(logs_amount * LogStructure.LOG_SIZE) if logs_amount is not None else file.read()


def print_logs(logs: List[Dict[str, Any]]) -> None:
    for log in logs:
        if log.get("error"):
            print(text_styles.red(log.__str__().strip("{}")))
        else:
            print(
                f'[ {(log.get("time", "?")) if not isinf(log.get("time")) else "inf"} ]  ',
                f'{text_styles.green((log.get("type") or "?").upper())}',
                text_styles.magenta(":"),
                f'{text_styles.green((log.get("source") or "?").upper())}  ',
                f'{text_styles.yellow(log.get("message") or "?")}  ',
                text_styles.blue(f'battery: {(log["charge"] if log.get("charge") is not None else "?")}%'),
                sep=''
            )


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument("path", help="path to logs file")
    parser.add_argument(
        "-o", "--offset",
        help="offset inside file to start from (in bytes), can be any valid integer (hex, bin and so on)",
        default=0, type=lambda x: int(x, 0)
    )
    parser.add_argument("-a", "--amount", help="amount of logs (not bytes) to read", default=None, type=int)
    args = parser.parse_args()
    if not os.path.isfile(args.path):
        print(f"Can not find '{args.path}' file", file=sys.stderr)
        exit(1)
    input_file = None
    try:
        input_file = open(args.path, 'rb')
    except PermissionError:
        print(f"Can not open '{args.path}' (permission denied)", file=sys.stderr)
        exit(1)
    print()
    print_logs(parse_logs(read_logs_from_file(input_file, args.offset, args.amount)))
    print()


if __name__ == "__main__":
    try:
        main()
    except (EOFError, KeyboardInterrupt):
        print("Exit")
